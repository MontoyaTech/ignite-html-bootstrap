import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, div, slot } from "../ignite-html/ignite-template.js";

class Modal extends IgniteElement {
    constructor() {
        super();
    }

    get properties() {
        return {
            dialogClasses: null,
            modal: null,
            modalInstance: null,
            backdropClose: true
        };
    }

    render() {
        return this.template.child(
            new div()
                .class("modal")
                .data("backdrop", this.backdropClose, (value) => !value ? "static" : null)
                .ref(this.modal)
                .child(
                    new div()
                        .class("modal-dialog")
                        .class(this.dialogClasses)
                        .child(
                            new div().class("modal-content").child(
                                new slot(this)
                            )
                        )
                )
        )
    }

    ready() {
        this.modalInstance = new bootstrap.Modal(this.modal, { backdrop: this.backdropClose ? "true" : "static" });
    }

    show() {
        this.modalInstance.show();
    }

    hide() {
        this.modalInstance.hide();
    }
}

customElements.define("bt-modal", Modal);

class ModalTemplate extends IgniteTemplate {
    constructor(...children) {
        super("bt-modal", children);
    }
}

export {
    ModalTemplate as Modal
}