import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, button, ul, slot, div } from "../ignite-html/ignite-template.js";

class DropdownMenu extends IgniteElement {
    constructor() {
        super();
    }

    get styles() {
        return /*css*/`
            bt-dropdown-menu {
                position: relative;
            }

            bt-dropdown-menu > div {
                position: relative;
            }

            bt-dropdown-menu > div.alignment-center {
                display: flex;
                flex-direction: column;
                align-items: center;
            }

            bt-dropdown-menu > div.alignment-center > ul {
                left: unset !important;
            }
        `;
    }

    get properties() {
        return {
            button: null,
            buttonClass: "btn",
            showButton: true,
            placement: "right",
            alignment: "none",
            minWidth: null,
            marginTop: null
        };
    }

    render() {
        return this.template.child(
            new div().class(this.alignment, value => "alignment-" + value).child(
                new button()
                    .class(this.buttonClass)
                    .data("toggle", "dropdown")
                    .show(this.showButton)
                    .child(this.button),
                new ul()
                    .class("dropdown-menu")
                    .class(this.placement, value => `dropdown-menu-${value}`)
                    .style("min-width", this.minWidth, "important", value => value ? value : "unset")
                    .style("margin-top", this.marginTop)
                    .child(new slot(this))
            )
        );
    }
}

class DropdownMenuTemplate extends IgniteTemplate {
    constructor(...children) {
        super("bt-dropdown-menu", children);
    }
}

customElements.define("bt-dropdown-menu", DropdownMenu);

export {
    DropdownMenuTemplate as DropdownMenu
};