import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, button } from "../ignite-html/ignite-template.js";

class DropdownButtonTemplate extends IgniteTemplate {
    constructor(...children) {
        super("button", children);

        this.class("dropdown-item");
        this.type("button");
    }
}

export {
    DropdownButtonTemplate as DropdownButton
};